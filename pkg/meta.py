#
# mkpdf - PDF creation from image files.
#
# Copyright (C) 2015      Daniel Henley <daniel.henley@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

VERSION_TUPLE = (0, 1, 0)
VERSION_QUALIFIER = ''
VERSION = '.'.join(map(str, VERSION_TUPLE)) + VERSION_QUALIFIER
major_version = '%s.%s' % (VERSION_TUPLE[0], VERSION_TUPLE[1])

AUTHOR = 'Daniel Henley'
AUTHOR_EMAIL = 'daniel.henley@gmail.com'
MAINTAINER = 'Daniel Henley'
MAINTAINER_EMAIL = 'daniel.henley@gmail.com'

NAME = 'mkpdf'
DESCRIPTION = ('mkpdf - PDF creation from image files.')
URL = 'https://bitbucket.org/wrongry/batch_photo_processor'
LICENSE = 'GPL v3'
PLATFORMS = ['Linux']
